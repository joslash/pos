package bootstrap.liftweb

import net.liftweb._
import util._
import Helpers._

import common._
import http._
import sitemap._
import Loc._
import mapper._

import code.model._
import net.liftmodules.FoBo

import scala.language.postfixOps

import net.joslash.pos.model._

import net.liftweb.sitemap.Loc.LocGroup

/**
 * A class that's instantiated early and run.  It allows the application
 * to modify lift's environment
 */
class Boot {
  def boot {
    if (!DB.jndiJdbcConnAvailable_?) {
      val vendor = 
	new StandardDBVendor(Props.get("db.driver") openOr "org.h2.Driver",
			     Props.get("db.url") openOr 
			     "jdbc:h2:lift_proto.db;AUTO_SERVER=TRUE",
			     Props.get("db.user"), Props.get("db.password"))

      LiftRules.unloadHooks.append(vendor.closeAllConnections_! _)

      DB.defineConnectionManager(util.DefaultConnectionIdentifier, vendor)
    }

    // Use Lift's Mapper ORM to populate the database
    // you don't need to use Mapper to use Lift... use
    // any ORM you want
    Schemifier.schemify(true, Schemifier.infoF _, User, Item, Stock, Client, Sales)

    // where to search snippet
    LiftRules.addToPackages("code")

    def sitemapMutators = User.sitemapMutator
    //The SiteMap is built in the Site object bellow 
    LiftRules.setSiteMapFunc(() => sitemapMutators(Site.sitemap))

    //Init the FoBo - Front-End Toolkit module, 
    //see http://liftweb.net/lift_modules for more info
    FoBo.InitParam.JQuery=FoBo.JQuery1102  
    FoBo.InitParam.ToolKit=FoBo.Bootstrap320 
    FoBo.init() 

    /*LiftRules.statelessRewrite.append(NamedPF("Client") {
        case RewriteRequest(
          ParsePath("admin" :: "client" :: client :: Nil,"",true,false),_,_) =>
        RewriteResponse(
          "admin" :: "client" :: Nil, Map("id" -> client))
      })
    LiftRules.statelessRewrite.append(NamedPF("itemEdit") {
        case RewriteRequest(
          ParsePath("admin" :: "item" :: "form" :: item :: Nil,"",true,false),_,_) =>
        RewriteResponse(
          "admin" :: "item" :: "form" :: Nil, Map("itemId" -> item))
      })*/
    
    //Show the spinny image when an Ajax call starts
    LiftRules.ajaxStart =
      Full(() => LiftRules.jsArtifacts.show("ajax-loader").cmd)
    
    // Make the spinny image go away when it ends
    LiftRules.ajaxEnd =
      Full(() => LiftRules.jsArtifacts.hide("ajax-loader").cmd)

    // Force the request to be UTF-8
    LiftRules.early.append(_.setCharacterEncoding("UTF-8"))

    // What is the function to test if a user is logged in?
    LiftRules.loggedInTest = Full(() => User.loggedIn_?)

    // Use HTML5 for rendering
    LiftRules.htmlProperties.default.set((r: Req) =>
      new Html5Properties(r.userAgent))    
      
    LiftRules.noticesAutoFadeOut.default.set( (notices: NoticeType.Value) => {
        notices match {
          case NoticeType.Notice => Full((8 seconds, 4 seconds))
          case _ => Empty
        }
     }
    ) 
    
    // Make a transaction span the whole HTTP request
    S.addAround(DB.buildLoanWrapper)
    MapperRules.createForeignKeys_? = (_) => true
  }
  
  object Site {
    import scala.xml._
    val divider1   = Menu("divider1") / "divider1"
    val ddLabel1   = Menu.i("UserDDLabel") / "ddlabel1"
    val home       = Menu.i("Home") / "index" 
    val admin      = Menu.i("Admin") / "admin" / "index"
    val items      = Menu.i("Artículos") / "admin" / "item"
    val itemsList  = Menu.i("Lista Artículos") / "admin" / "item" / "list"
    val itemsCreate = Menu.i("Nuevo Artículo") / "admin" / "item" / "create" >> Hidden
    val itemsEdit  = Menu.i("Editar Artículo") / "admin" / "item" / "edit" >> Hidden
    val itemsDelete = Menu.i("Borrar Artículo") / "admin" / "item" / "delete" >> Hidden
    val clients      = Menu.i("Clientes") / "admin" / "client"
    val clientsList  = Menu.i("Lista Clientes") / "admin" / "client" / "list"
    val clientsCreate = Menu.i("Nuevo Cliente`") / "admin" / "client" / "create" >> Hidden
    val clientsEdit  = Menu.i("Editar Cliente") / "admin" / "client" / "edit" >> Hidden
    val clientsDelete = Menu.i("Borrar Cliente") / "admin" / "client" / "delete" >> Hidden
    val stock        = Menu.i("Stock") / "admin" / "stock"
    val stockList  = Menu.i("Lista Stock") / "admin" / "stock" / "list"
    val stockCreate = Menu.i("Nuevo Stock`") / "admin" / "stock" / "create" >> Hidden
    val stockEdit  = Menu.i("Editar Stock") / "admin" / "stock" / "edit" >> Hidden
    val stockDelete = Menu.i("Borrar Stock") / "admin" / "stock" / "delete" >> Hidden
    val pos = Menu.i("Ventas") / "pos"
    val posCash = Menu.i("Caja") / "pos" / "cash"
                        
    val userMenu   = User.AddUserMenusHere
    val static     = Menu(Loc("Static", Link(List("static"), true, "/static/index"), S.loc("StaticContent" , scala.xml.Text("Static Content")),LocGroup("lg2","topRight")))
    val twbs  = Menu(Loc("twbs", 
        ExtLink("http://getbootstrap.com/"), 
        S.loc("Bootstrap3", Text("Bootstrap3")), 
        LocGroup("lg2"),
        FoBo.TBLocInfo.LinkTargetBlank ))     
    
    
    def sitemap = SiteMap(
        home          >> LocGroup("lg1"),
        admin         >> LocGroup("admin"),
        items         >> LocGroup("admin") >> 
                          PlaceHolder submenus (
                            itemsList, itemsCreate, itemsEdit, itemsDelete
                          ),
        clients       >> LocGroup("admin") >> 
                          PlaceHolder submenus (
                            clientsList, clientsCreate, clientsEdit, clientsDelete
                          ),
        stock         >> LocGroup("admin") >> 
                          PlaceHolder submenus (
                            stockList, stockCreate, stockEdit, stockDelete
                          ),
        pos           >> LocGroup("admin") >> 
                          PlaceHolder submenus (
                            posCash
                          ),
        static,
        twbs,
        ddLabel1      >> LocGroup("topRight") >> PlaceHolder submenus (
            divider1  >> FoBo.TBLocInfo.Divider >> userMenu
            )
         )
  }
  
}
