package net.joslash.pos.model

import net.liftweb.mapper._
import scala.xml.NodeSeq
import _root_.java.math.MathContext
import net.liftweb.sitemap.Loc.LocGroup

import net.liftweb.common.Logger

object ItemStatus extends Enumeration {
	val ACTIVE = Value(1,"Activo")
	val CANCELED = Value(2,"Cancelado")
}

object Item extends Item 
	with LongKeyedMetaMapper[Item]
	with CRUDify[Long, Item]{
	
	override def dbTableName = "items"
}

class Item extends LongKeyedMapper[Item]
	with IdPK
	with CreatedUpdated 
	with OneToMany[Long, Item] 
	with Logger{
		def getSingleton = Item
		object name extends MappedString(this, 200) {
			override def dbIndexed_? = true
			override def dbNotNull_? = true
			override def required_? = true
			override def validations = 
				valMinLen(3, "El nombre del artículo debe ser de al menos 3 caracteres") _ ::
				valUnique("Este articulo ya fue dado de alta con anterioridad") _ :: 
				super.validations
		}
		object price extends MappedDecimal(this, MathContext.DECIMAL64, 3) {
			override def dbNotNull_? = true
			override def required_? = true
		}
		object status extends MappedEnum(this, ItemStatus) {
			override def defaultValue = ItemStatus.ACTIVE
			override def dbNotNull_? = true
		}
		object stock extends MappedOneToMany(Stock, Stock.item,OrderBy(Stock.item, Ascending))
			with Cascade[Stock]
			with Owned[Stock] 

		def itemList = {
			Item.findAll(OrderBy(Item.name, Ascending))
		}
}