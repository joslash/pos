package net.joslash.pos.model

import net.liftweb.mapper._
import net.liftweb.util.FieldError

import net.liftweb.common.Logger

object StockTransaction extends Enumeration {
	val ADD = Value(1,"Entrada")
	val SUBTRACT = Value(2,"Salida")
}

object Stock extends Stock 
	with LongKeyedMetaMapper[Stock] 
	with CRUDify[Long, Stock]{

	override def dbTableName = "stock"
}

class Stock extends LongKeyedMapper[Stock]
	with IdPK
	with CreatedUpdated 
	with Logger{
		def getSingleton = Stock
		//La cantidad se guarda positiva(ingreso) o negativa(egreso)
		object amount extends MappedInt(this) {
			def validateAmount (amount : Int) = {
				if(amount == 0){
					List(FieldError(this, "La cantidad a registrar debe ser distinta a 0"))
				}
				else{
					List[FieldError]()
				}
			}
			override def dbNotNull_? = true
			override def required_? = true
			override def validations = 
				validateAmount _ ::
				super.validations
		}
		object transaction extends MappedEnum(this, StockTransaction) {
			override def defaultValue = StockTransaction.ADD
			override def dbNotNull_? = true
		}
		object item extends MappedLongForeignKey(this, Item) {
			override def dbForeignKey_? = true
			override def dbIndexed_? = true
			override def dbNotNull_? = true
		}

		def getStockList(item :Item) = {
			Stock.findAll(By(Stock.item, item.id), OrderBy(Stock.createdAt, Descending))
		}

		def getStockActual(item :Item) = {
			

			val stock :List[Int]= getStockList(item).map ( (t:Stock) => 
				t.transaction.toString match{
						case "Entrada" => (t.amount).toInt
						case "Salida" => (t.amount * -1).toInt
					}
				)
			stock.sum
		}
}