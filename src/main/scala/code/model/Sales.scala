package net.joslash.pos.model

import net.liftweb.mapper._
import _root_.java.math.MathContext
import net.liftweb.common.Logger

object DiscountType extends Enumeration {
	val MONEY = Value(1,"Efectivo")
	val PERCENTAGE = Value(2,"Porcentaje")
}

object Sales extends Sales with LongKeyedMetaMapper[Sales] {
	override def dbTableName = "sales"
}

class Sales extends LongKeyedMapper[Sales]
	with IdPK
	with CreatedUpdated 
	with OneToMany [Long, Sales] 
	with Logger{
		def getSingleton = Sales
		object cost extends MappedDecimal(this, MathContext.DECIMAL64, 3) {
			override def dbNotNull_? = true
			override def required_? = true
		}
		object amount extends MappedDecimal(this, MathContext.DECIMAL64, 3) {
			override def dbNotNull_? = true
			override def required_? = true
		}
		object discount extends MappedDecimal(this, MathContext.DECIMAL64, 3) {
			override def dbNotNull_? = true
			override def required_? = true
		}
		object discType extends MappedEnum(this, DiscountType) {
			override def defaultValue = DiscountType.MONEY
			override def dbNotNull_? = true
		}
		object subtotal extends MappedDecimal(this, MathContext.DECIMAL64, 3) {
			override def dbNotNull_? = true
		}
		object tax extends MappedDecimal(this, MathContext.DECIMAL64, 3) {
			override def dbNotNull_? = true
		}
		object total extends MappedDecimal(this, MathContext.DECIMAL64, 3) {
			override def dbNotNull_? = true
		}
		object status extends MappedString(this, 2) {
			override def defaultValue = "A"
			override def dbNotNull_? = true
		}
		object item extends MappedLongForeignKey(this, Item)
		object client extends MappedLongForeignKey(this, Client)

	
}