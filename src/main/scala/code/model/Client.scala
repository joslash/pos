package net.joslash.pos.model

import net.liftweb.mapper._
import net.liftweb.util.FieldError

import net.liftweb.common.Logger

object Client extends Client 
	with LongKeyedMetaMapper[Client]
	with CRUDify[Long, Client]{
	override def dbTableName = "clients"
}

class Client  extends LongKeyedMapper[Client]
	with IdPK
	with CreatedUpdated 
	with OneToMany [Long, Client] 
	with Logger{
		def getSingleton = Client 
		object name extends MappedString(this, 250) {
			override def dbIndexed_? = true
		    override def dbNotNull_? = true
		    override def required_? = true
		    override def validations = 
				valMinLen(3, "El nombre del cliente debe ser de al menos 3 caracteres") _ ::
				valUnique("Este cliente ya fue dado de alta con anterioridad") _ :: 
				super.validations
		}
		object rfc extends MappedString(this, 20) {
			def validateRFCLength (rfc : String) = {
				if(rfc.length < 10){
					List(FieldError(this, "El RFC debe ser al menos de 10 caracteres!"))
				}
				else if(rfc.length > 13){
					List(FieldError(this, "El RFC de ser maximo de 13 caracteres!"))
				}
				else{
					List[FieldError]()
				}
			}
			override def validations = validateRFCLength _ :: 
				valUnique("Un cliente con este RFC ya existe") _ :: super.validations
			override def dbNotNull_? = true
			override def dbIndexed_? = true
		    override def required_? = true
		}
		object status extends MappedEnum(this, ItemStatus) {
			override def defaultValue = ItemStatus.ACTIVE
			override def dbNotNull_? = true
		}
		object sales extends MappedOneToMany(Sales, Sales.client,OrderBy(Sales.createdAt, Ascending))
			with Cascade[Sales]
			with Owned[Sales]

		def clientList = {
			Client.findAll(OrderBy(Client.name, Ascending))
		}
} 