package code.snippet

import net.liftweb.http.{S,SHtml,RequestVar}
import net.liftweb.common.{Logger,Full, Box, Empty, Failure}
import net.liftweb.util.Helpers._
import scala.xml.{NodeSeq,Text}
import net.liftweb.mapper._

import net.joslash.pos.model.{Stock,Item,ItemStatus, StockTransaction}
import code.snippet.itemSes
import code.snippet.itemReq

object stockReq extends RequestVar[Box[Stock]](Empty)
class StockSn extends Logger with StockHelpers {

	def processSubmit() = {
		debug("Entrando al metodo 'processSubmit'")
		val item = itemSes.is openOr(new Item)
        val stock = stockReq.is openOr(new Stock)
	    stock.validate match {
	      case Nil => {
	        //val usuario = User.currentUser.head
	        //stockReq.is.usuario(usuario.id)
	        
	        stock.item(item.id)
	        stock.save
	        S.notice("Se guardó el stock de '"+stock.item.obj.get.name+"' con éxito!")
	        S.seeOther("/admin/stock/list")
	      }
	      case errors => S.error(errors)
	    }
	}

	def list = {
		debug("Entrando al metodo 'List'")

		val itemF : Box[Item] = itemReq.is or itemSes or Empty
		debug("Item "+itemF)

		val item = itemF.get
		itemSes.set(itemF)

		val sdf = new java.text.SimpleDateFormat("dd/MM/yyyy")

		".fila *" #> Stock.getStockList(item).map{ t => 
	    ".stock-transaction *" #> Text(t.transaction.get.toString) &
	    ".stock-amount *" #> Text(t.amount.get.toString) &
	    ".stock-created *" #> Text(sdf.format(t.createdAt.get))
	    }	

    	
	}

	def create = {
		debug("Entrando al metodo 'Create'")
		val item = itemSes.is openOr(new Item)
		val stock = stockReq.is openOr(new Stock)

		debug("item "+item)
		debug("stock "+stock) 

		object transaction extends RequestVar[StockTransaction.Value](StockTransaction.ADD)

		"#hidden" #> SHtml.hidden(() => stockReq.set(Full(stock))) &
		"#stock-amount" #> SHtml.text(stock.amount.get.toString, amount => stockReq.set(Full(stock.amount(amount.toInt)))) &
		"#stock-transaction" #> SHtml.selectObj[StockTransaction.Value](StockTransaction.values.toList.map(v => (v, v.toString)), Full(transaction.is), tr => stockReq.set(Full(stock.transaction(tr)))) &
 		"#submit" #> SHtml.onSubmitUnit(processSubmit)
	}

	def stockNumItems = {
		debug("Entrando al metodo 'stockNumItems'")

		val itemF : Box[Item] = itemReq.is or itemSes or Empty
		debug("Item "+itemF)

		val item = itemF.get
		
		Text(itemStockActual(item).toString)
	}	

}

trait StockHelpers extends Logger {
	protected def itemStockActual(item :Item) = {
		debug("Entrando al metodo 'itemStockActual'")
		
		Stock.getStockActual(item)
	}	
}