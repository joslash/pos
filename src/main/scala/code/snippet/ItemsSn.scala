package code.snippet

import net.liftweb.http.{S,SHtml,RequestVar,SessionVar}
import net.liftweb.http.js.{JsCmd, JsCmds}
import net.liftweb.http.js.JsCmds.{Alert, Run, Script,Confirm, RedirectTo, JsReturn, JsIf, CmdPair, After}
import net.liftweb.http.js.JE.{JsRaw,JsEq, ValById}
import net.liftweb.common.{Logger,Full, Box, Empty}
import net.liftweb.util.Helpers._
import scala.xml.{NodeSeq,Text}
import net.liftweb.mapper._
import net.liftweb.http.SHtml.{SelectableOption}

import net.joslash.pos.model.{Item,ItemStatus}


object itemSes extends SessionVar[Box[Item]](Empty)
object itemReq extends RequestVar[Box[Item]](Empty)

class ItemsSn extends Logger with StockHelpers{
	
	
	def processSubmit() = {
		val item = itemReq.is openOr(new Item)
	    item.validate match {
		    case Nil => {
		        //val usuario = User.currentUser.head
		        //itemReq.is.usuario(usuario.id)
		        
		        item.save
		        S.notice("Se guardo el Artículo '"+item.name+"' con exito!")
		        S.seeOther("/admin/item/list")
		    }
		    case errors => S.error(errors)
	    }
	}

	def list = {
		def callback(item : Item) : JsCmd = {
			debug("The button was pressed")
			//JsCmds.Alert("You clicked it")
			CmdPair(
			JsCmds.Run("""swal({
				  title: "Are you sure?",
				  text: "Item will be deleted!",
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonColor: "#DD6B55",
				  confirmButtonText: "Yes, delete it!",
				  closeOnConfirm: false,
				  showLoaderOnConfirm: true
				},
				function(){"""
				+
					SHtml.ajaxInvoke(() => {debug(item);item.delete_!;S.notice("Item was deleted")})
				+
				""";setTimeout(function() {swal("Item was deleted!");}, 2000);

				}
				)"""),
			JsCmds.After(5 seconds, JsCmds.Reload)
			)
		}

	    ".fila *" #> Item.findAll.map{ t => 
		    ".item-name *" #> Text(t.name.get) &
		    ".item-price *" #> Text(t.price.get.toString) &
		    ".item-stock *" #> SHtml.link("/admin/stock/list", () => itemReq(Full(t)), Text(itemStockActual(t).toString), "class" -> "badge btn btn-default", "aria-hidden" -> "true", "title" -> "Stock") &
		    ".item-status *" #> Text(t.status.toString) &
		    ".actions *" #> {
		    SHtml.link("/admin/item/edit", () => itemReq(Full(t)), Text(""), "class" -> "glyphicon glyphicon-pencil btn btn-info", "aria-hidden" -> "true", "title" -> "Editar") ++ Text(" ") ++
		    SHtml.a(Text(" "), callback(t), "class" -> "glyphicon glyphicon-trash btn btn-danger", "aria-hidden" -> "true", "title" -> "Borrar") ++ Text(" ")
		    
			}
	    } 
	}

	def create = {
		val item = itemReq.is openOr(new Item)

		object status extends RequestVar[ItemStatus.Value](ItemStatus.ACTIVE)

		"#hidden" #> SHtml.hidden(() => itemReq.set(Full(item))) &
		"#item-name" #> SHtml.text(item.name.get, name => itemReq.set(Full(item.name(name)))) &
		"#item-price" #> SHtml.text(item.price.get.toString, price => itemReq.set(Full(item.price(BigDecimal(price))))) &
		"#item-status" #> SHtml.selectObj[ItemStatus.Value](ItemStatus.values.toList.map(v => (v, v.toString)), Full(status.is), st => itemReq.set(Full(item.status(st)))) &
 		"#submit" #> SHtml.onSubmitUnit(processSubmit)


	}

	def edit = {
	    if ( ! itemReq.set_? )
	    	S.redirectTo("/admin/item/list")

	    val item = itemReq.is openOr(new Item)
	    object status extends RequestVar[ItemStatus.Value](ItemStatus.ACTIVE)
	    
	    "#hidden" #> SHtml.hidden(() => itemReq.set(Full(item)) ) &
	    "#item-name" #> SHtml.text(item.name.get, name => itemReq.set(Full(item.name(name)))) &
	    "#item-price" #> SHtml.text(item.price.get.toString, price => itemReq.set(Full(item.price(BigDecimal(price))))) &
	    "#item-status" #> SHtml.selectObj[ItemStatus.Value](ItemStatus.values.toList.map(v => (v, v.toString)), Full(status.is), st => itemReq.set(Full(item.status(st)))) &
	    "#submit" #> SHtml.onSubmitUnit(processSubmit)
    }

    def delete = {
	    if ( ! itemReq.set_? ){
	    	S.error("parameter is missing");
	      	S.redirectTo("/admin/item/list")
	  	}

	  	val item = itemReq.get
	    
    	"#item" #> item.get.name &
	    "#yes" #> SHtml.link("/admin/item/list?itemId=" +item.get.id, () => {item.get.delete_!}, Text("Si"), "class" -> "btn btn-default") &
	    "#no" #> SHtml.link("/admin/item/list?itemId=" +item.get.id, () => { }, Text("No"), "class" -> "btn btn-danger")
	    
	}

	def itemName = {
		debug("Entrando al metodo 'itemName'")

		val itemF : Box[Item] = itemReq.is or itemSes or Empty
		debug("Item "+itemF)

		val item = itemF.get
		Text(item.name)
	}



}

