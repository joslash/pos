package code.snippet

import net.liftweb.http.{S,SHtml,RequestVar, SessionVar, WiringUI}
import net.liftweb.http.SHtml.SelectableOption
import net.liftweb.common.{Logger,Full, Box, Empty, Failure}
import net.liftweb.util.Helpers._
import scala.xml.{NodeSeq,Text}
import net.liftweb.mapper._	

import net.liftweb.util.ValueCell
import net.liftweb.http.js.JsCmds.Noop
import net.liftweb.http.js.jquery.JqWiringSupport

import scala.xml.{NodeSeq, Text}

import net.joslash.pos.model.{Item,Sales,Client}

object salesReq extends RequestVar[Box[Sales]](Empty)
//object shoppingCart extends SessionVar[Box[List[Sales]]](Empty)
object shoppingCart extends SessionVar[ValueCell[List[Sales]]](ValueCell(List(new Sales)))

class SalesSn extends Logger with StockHelpers {

	private object NewSale {
		debug("Last element "+shoppingCart.get)
		val list = shoppingCart.get

		val latestElement = ValueCell(new Sales)
		if(list.length > 0){
			latestElement.set(list(0))
		}
		
		val subtotal = latestElement.lift(cantidadValue){
			debug("Price "+ latestElement.item.obj.get.price)
			debug("cantidadValue "+ cantidadValue)
			_.item.obj.get.price.toDouble * _.openOr(0d)
		}
	}

	private val precioValue = ValueCell[Box[Double]](Full(0d))
	private val cantidadValue = ValueCell[Box[Double]](Full(0d))
	private val descuentoValue = ValueCell[Box[Double]](Full(0d))
	private val subtotalValue = ValueCell[Box[Double]](Full(0d))

	def cash(xhtml :NodeSeq) = {
		debug("Entrando al metodo 'Cash'")

		val sdf = new java.text.SimpleDateFormat("dd/MM/yyyy")
		val sales = salesReq.openOr (new Sales)
		//var precio = new java.math.BigDecimal("0")

		bind("cf", xhtml,
			"cliente" -> SHtml.selectObj[Client](Client.clientList.map((t) => new SelectableOption(t, t.name)), Empty, cl => salesReq.set(Full(sales.client(cl))), "class" -> "form-control"),
			"articulo" -> SHtml.selectObj[Item](Item.itemList.map((t) => new SelectableOption(t, t.name)), Empty, it => salesReq.set(Full(sales.item(it))), "class" -> "form-control"),
			"submit" -> SHtml.submit("Agregar", () => addSale(xhtml :NodeSeq), "class" -> "btn btn-default pull-right")
		)
	}
	
	def cantidad = {
		debug("Asignando cantidad")
		"#cantidad" #> SHtml.ajaxText(
			cantidadValue.map(_.toString).openOr("0.00"),
			p => { asDouble(p).pass(
				cantidadValue.set(_)) 
				Noop
			}
	)}

	def subtotal(in :NodeSeq) = WiringUI.asText(in, NewSale.subtotal)

	private def doubleDraw: (Box[Double], NodeSeq) => NodeSeq = 
	    (d, ns) => Text(java.text.NumberFormat.getCurrencyInstance.format(d.openOr(0d)))

	def addSale(xhtml :NodeSeq) = {
		debug("Entando al metodo 'addSale'")

		val sales = salesReq openOr (new Sales)
		debug("Sales "+sales)

/*		val list = shoppingCart openOr (List())
		debug("shoppingCart "+list)

		shoppingCart.set(Full(list ::: List(new Sales().client(sales.client).item(sales.item).cost(sales.cost).amount(sales.amount).discount(sales.discount)
			.subtotal(sales.subtotal).tax(sales.tax).total(sales.total))))
		debug("shoppingCart "+shoppingCart.is)
*/
		salesReq.set(Full(sales))
	}
}


