package code.snippet

import net.liftweb.http.{S,SHtml,RequestVar}
import net.liftweb.common.{Logger,Full, Box, Empty}
import net.liftweb.util.Helpers._
import scala.xml.{NodeSeq,Text}
import net.liftweb.mapper._

import net.joslash.pos.model.{Client,ItemStatus}

class ClientsSn extends Logger {
	object clientReq extends RequestVar[Client](Client.create)

	def processSubmit() = {
	    clientReq.is.validate match {
	      case Nil => {
	        clientReq.is.save
	        S.notice("Se guardo el Cliente '"+clientReq.is.name+"' con exito!")
	        S.seeOther("/admin/client/list")
	      }
	      case errors => S.error(errors)
	    }
	}

	def list = {

	    ".fila *" #> Client.findAll.map{ t => 
		    ".client-name *" #> Text(t.name.get) &
		    ".client-rfc *" #> Text(t.rfc.get) &
		    ".client-status *" #> Text(t.status.toString) &
		    ".actions *" #> {
		    SHtml.link("/admin/client/edit", () => clientReq(t), Text("Editar"), "class" -> "glyphicon glyphicon-pencil btn btn-info", "aria-hidden" -> "true") ++ Text(" ") ++
		    SHtml.link("/admin/client/delete", () => clientReq(t), Text("Borrar"), "class" -> "glyphicon glyphicon-trash btn btn-danger")
			}
	    } 
	}

	def create = {
		val client = clientReq.is

		object status extends RequestVar[ItemStatus.Value](ItemStatus.ACTIVE)

		"#hidden" #> SHtml.hidden(() => clientReq(client)) &
		"#client-name" #> SHtml.text(clientReq.name.get, name => clientReq.is.name(name)) &
		"#client-rfc" #> SHtml.text(clientReq.rfc.get, rfc => clientReq.is.rfc(rfc)) &
		"#client-status" #> SHtml.selectObj[ItemStatus.Value](ItemStatus.values.toList.map(v => (v, v.toString)), Full(status.is), st => clientReq.status(st)) &
 		"#submit" #> SHtml.onSubmitUnit(processSubmit)

	}

	def edit = {
	    if ( ! clientReq.set_? )
	    	S.redirectTo("/admin/client/list")

	    val client = clientReq.is
	    object status extends RequestVar[ItemStatus.Value](ItemStatus.ACTIVE)
	    
	    "#hidden" #> SHtml.hidden(() => clientReq(client) ) &
	    "#client-name" #> SHtml.text(clientReq.name.get, name => clientReq.is.name(name) ) &
	    "#client-rfc" #> SHtml.text(clientReq.rfc.get, rfc => clientReq.is.rfc(rfc)) &
	    "#client-status" #> SHtml.selectObj[ItemStatus.Value](ItemStatus.values.toList.map(v => (v, v.toString)), Full(status.is), st => clientReq.status(st)) &
	    "#submit" #> SHtml.onSubmitUnit(processSubmit)
    }

    def delete = {
	    if ( ! clientReq.set_? )
	      S.redirectTo("/admin/client/list")

	    val client = clientReq.is  

	    "#client" #> client.name &
	    "#yes" #> SHtml.link("/admin/client/list?itemId=" +client.id.get, () => {client.delete_!}, Text("Si"), "class" -> "btn btn-default") &
	    "#no" #> SHtml.link("/admin/client/list?itemId=" +client.id.get, () => { }, Text("No"), "class" -> "btn btn-danger")
	}
}